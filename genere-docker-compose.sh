#!/usr/bin/env bash
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io
# http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace
# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app
arg1="${1:-}"

export DB_HOST="db"
export DB_PATH="/data/mysql"
export DB_PORT=3306
export DB_NAME="rancherdb"
export DB_USER="root"
export DB_PASS="DvHSb5042jTL"
export JDBC_URL="jdbc:mysql://$DB_HOST:$DB_PORT/$DB_NAME?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8&prepStmtCacheSize=517&cachePrepStmts=true&prepStmtCacheSqlLimit=4096&socketTimeout=60000&connectTimeout=60000&sslServerCert=/var/lib/rancher/etc/ssl/ca.pem&useSSL=true"


cat <<EOF > docker-compose.yml
version: '3.1'
services:
  db:
    image: mysql:8.0
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: $DB_PASS
    volumes:
      - $DB_PATH:/var/lib/mysql
    ports:
      - "3306:3306"
  rancher-server:
    image: rancher/server:stable
    restart: unless-stopped
    command: --db-host $DB_HOST --db-port $DB_PORT --db-name $DB_NAME --db-user $DB_USER --db-pass $DB_PASS
    environment:
      CATTLE_DB_LIQUIBASE_MYSQL_URL: $JDBC_URL
      CATTLE_DB_CATTLE_MYSQL_URL: $JDBC_URL
      CATTLE_DB_CATTLE_GO_PARAMS: "tls=true"
    volumes:
      - $DB_PATH/ca.pem:/var/lib/rancher/etc/ssl/ca.pem
    ports:
      - "8081:8080"
EOF
