path=~/domain.fr/
ssh=jl31u_droapp@domain.fr
domain=domain.fr

all: install
	$(MAKE) run

help: ## Affiche cette aide
    @grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## Genere le docker-compose
	. ./genere-docker-compose.sh

run: up-db up-server

up: run

up-db:
	docker-compose up -d db

up-server:
	docker-compose up -d rancher-server

stop:
	docker-compose down

down: stop

clean: down ## delete
	sudo rm -rf /data/mysql

logs:
	docker-compose logs -f

logs-server:
	docker-compose logs --tail 33 -f rancher-server

logs-db:
	docker-compose logs --tail 33 -f db

exec-server:
	docker-compose exec rancher-server bash

ps:	
	docker-compose ps

test:
	docker run -d --name=miam --restart=always -p 8082:8080 rancher/server:stable
